# CyberTerrain
 C&C server for CTN simulation.


### Commands: 

### register_installed_malware

curl -X POST localhost:5000/register_installed_malware -H 'Content-Type: application/json' -d '{"os_type":"linux","ip":"10.0.0.8","pretty_name":"alex"}'


{
  "ip": "10.0.0.8", 
  "jwt": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwdWJsaWNfaWQiOiJmYTkzMjE2MS1hMjdiLTQ2OWQtYjRmMy03MjRkZDAzNTM4YjYiLCJleHAiOjE2NTQ1MjY5NDR9.qqqHbDRU0BxGip0BitkymdyJ88AalR6bWsg9b9D9jGg", 
  "last_seen": "2022-05-07T17:49:04.590547", 
  "os_type": "linux", 
  "pretty_name": "alex", 
  "setup_time": "2022-05-07T17:49:04.590544", 
  "user_id": 1
}



### pull_commands_requests

curl -X POST localhost:5000/pull_commands_requests -H 'Content-Type: application/json' -H 'x-access-token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwdWJsaWNfaWQiOiI4Y2RhYzI5Yi04MjE2LTQ2Y2EtODgzNy0zNWE1YzNjNTJkMmMiLCJleHAiOjE2NTY3Nzc5MTd9.XP1sLbNQuWnhlIwS-oiDlvtizv7W-E0pizzGO95hGic'


[{'commandId': 1, 'commandType': 3, 'commandPayload': None}]



### set_command_status


curl -X POST localhost:5000/set_command_status -H 'Content-Type: application/json' -H 'x-access-token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwdWJsaWNfaWQiOiI1YjQzZWE1MS0wNWU2LTQ3MjYtYmNjNS0wMTVlOTUwNDZiMWIiLCJleHAiOjE2NTY4MzkwNzV9.-ueqngzKFFDoWEeH_c1WRzGysFKBnlrEEdrG-RO6ClM' -d '{"command_request_id": "1", "command_request_result": "Applications", "command_request_error": "non", "command_request_succeeded": "True"}'


True


### upload_file

1 = command_request_id

curl -X POST localhost:5000/upload_file/1 -F 'file=@test.py' -i -H 'x-access-token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwdWJsaWNfaWQiOiI2N2EwMTcxZC04ZjA2LTQxNDEtOGYzMy05ZDJiNTRmYzJmYzgiLCJleHAiOjE2NTY5NDQ2Nzl9.7LIW9twhPTM9t1378KzuKCSY8kMzPtEdXNGphVbxzGs'


HTTP/1.1 200 OK
Server: Werkzeug/2.1.2 Python/3.9.12
Date: Sat, 04 Jun 2022 14:55:45 GMT
Content-Type: application/json
Content-Length: 74
Connection: close

{"filename":"test.py","message":"test.py saved successful.","status":200}

**I hope you enjoy it.**
